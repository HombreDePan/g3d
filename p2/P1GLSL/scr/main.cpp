#include "BOX.h"
#include <IGL/IGlib.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>


//Idenficadores de los objetos de la escena
int objId =-1;

//Declaraci�n de CB
void resizeFunc(int width, int height);
void idleFunc();
void keyboardFunc(unsigned char key, int x, int y);
void mouseFunc(int button, int state, int x, int y);
void mouseMotionFunc(int x, int y);

//Movimiento de la c�mara
glm::vec3 cam_pos(0.0f, 0.0f, 6.0f);
glm::vec3 cam_forward(0.0f, 0.0f, -1.0f);
glm::vec3 cam_up(0.0f, 1.0f, 0.0f);

float rotation_speed = 0.05f;
float speed = 0.1f;
float cam_horizontal_rotation = 0;
float cam_vertical_rotation = 0;

int main(int argc, char** argv)
{
	std::locale::global(std::locale("spanish"));// acentos ;)
	if (!IGlib::init("../shaders_P1/shader.v2.vert", "../shaders_P1/shader.v2.frag"))
		return -1;
   
	//CBs
	IGlib::setResizeCB(resizeFunc);
	IGlib::setIdleCB(idleFunc);
	IGlib::setKeyboardCB(keyboardFunc);
	IGlib::setMouseCB(mouseFunc);
  	IGlib::setMouseMoveCB(mouseMotionFunc);

	//Se ajusta la c�mara
	float n = 1.0f;
	float f = 50.0f;
	glm::mat4 view(1.0f);
	glm::mat4 proj(0.0f);
	
	//view[3].z = -6.0f;
	
	//NOTA: PREGUNTARLE A MARCOS EL USO DE LOOKAT
	view = glm::lookAt(cam_pos, cam_forward, cam_up);
	
	proj[0].x = 1.0f / 
			tan(3.14159f / 6.0f);
	proj[1].y = proj[0].x;
	proj[2].z = -(n + f) / (f - n);
	proj[3].z = -2.0f * f * n / (f - n);
	proj[2].w = -1.0f;
	
	
	IGlib::setProjMat(proj);
	IGlib::setViewMat(view);

	//Creamos el objeto que vamos a visualizar
	objId = IGlib::createObj(cubeNTriangleIndex, cubeNVertex, cubeTriangleIndex, 
			cubeVertexPos, cubeVertexColor, cubeVertexNormal,cubeVertexTexCoord, cubeVertexTangent);
		
	glm::mat4 modelMat = glm::mat4(1.0f);
	IGlib::setModelMat(objId, modelMat);

	//Incluir texturas aqu�.
	IGlib::addColorTex(objId, "../img/color2.png");
	IGlib::addEmissiveTex(objId, "../img/emissive.png");
	IGlib::addSpecularTex(objId, "../img/specMap.png");
	IGlib::addNormalTex(objId, "../img/normal.png");	
	
	//Mainloop
	IGlib::mainLoop();
	IGlib::destroy();
	return 0;
}

void resizeFunc(int width, int height)
{
	//Ajusta el aspect ratio al tama�o de la venta
}

void idleFunc()
{
	static float angle = 0.0f;

	angle = (angle<2.0f*3.14159f)?
		angle + 0.01f : 0.0f;

	glm::mat4 model(1.0f);

	model = glm::rotate(model, angle, glm::vec3(1.0f, 1.0f, 0.0f));

	IGlib::setModelMat(objId, model);
}

void keyboardFunc(unsigned char key, int x, int y)
{
	std::cout << "Se ha pulsado la tecla " << key << std::endl << std::endl;

	glm::mat4 view(1.0f);

	if (key == 'w')
	{
		//Adelante
		cam_pos += speed * cam_forward;
	}
	if (key == 'a')
	{
		//Izquierda
		cam_pos -= glm::normalize(glm::cross(cam_forward, cam_up)) * speed;
	}
	if (key == 's')
	{
		//Atr�s
		cam_pos -= speed * cam_forward;
	}
	if (key == 'd')
	{
		//Derecha
		cam_pos += glm::normalize(glm::cross(cam_forward, cam_up)) * speed;
	}
	if (key == 'e')
	{
		//Rotaci�n derecha
		//cam_forward = rotation_speed;
		cam_forward = glm::vec3(glm::rotate(glm::mat4(1.0f), -rotation_speed, glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(cam_forward, 0));
	}
	else if (key == 'q')
	{
		//Rotaci�n izquierda
		cam_horizontal_rotation -= rotation_speed;
		cam_forward = glm::vec3(glm::rotate(glm::mat4(1.0f), rotation_speed, glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(cam_forward,0));
		//view = glm::rotate(view, cam_horizontal_rotation, glm::vec3(0.0f, 1.0f, 0.0f));
	}

	view = glm::lookAt(cam_pos, cam_pos + cam_forward, cam_up);

	IGlib::setViewMat(view);
}

void mouseFunc(int button, int state, int x, int y)
{
	if (state==0)
		std::cout << "Se ha pulsado el bot�n ";
	else
		std::cout << "Se ha soltado el bot�n ";
	
	if (button == 0) std::cout << "de la izquierda del rat�n " << std::endl;
	if (button == 1) std::cout << "central del rat�n " << std::endl;
	if (button == 2) std::cout << "de la derecha del rat�n " << std::endl;

	std::cout << "en la posici�n " << x << " " << y << std::endl << std::endl;
}

void mouseMotionFunc(int x, int y)
{

}
