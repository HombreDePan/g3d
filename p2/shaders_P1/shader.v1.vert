#version 330 core

in vec3 inPos;
in vec3 inColor;	
in vec3 inNormal;

uniform mat4 modelViewProj;
uniform mat4 normal;
uniform mat4 modelView;


out vec3 pv;
out vec3 nv;

void main()
{
	pv = (modelView * vec4(inPos, 1.0)).xyz;;     
	nv = normalize((normal * vec4(inNormal,0)).xyz);
	
	gl_Position = modelViewProj * vec4(inPos, 1.0);
}
