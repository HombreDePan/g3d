#version 330 core

in vec3 pv;
in vec3 nv;

out vec4 outColor;



//Parametros de la luz
vec3 Il = vec3(1.0);
vec3 Ia = vec3(0.3);
vec3 pl = vec3(0,0,0);

//Obj
vec3 Ka;
vec3 Kd;
vec3 Ks;
float brightness;
vec3 Ke;
vec3 pos;

vec3 N;



vec3 shade()
{
	vec3 c = vec3(0);

	c+= Ia*Ka;

	vec3 L = normalize(pl-pos);
	c+= clamp (Il*Kd*dot(N,L),0.0,1.0);

	vec3 V = normalize(-pos);
	vec3 R = normalize (reflect(-L,N));
	float Isfactor = clamp (dot(V,R), 0.0, 1.0);
	Isfactor = pow(Isfactor, brightness);
	c+= Il*Ks*Isfactor;
		
	return c;
}



void main()
{
	Ka = vec3(1,0,0);
	Kd = Ka;
	Ks = vec3(1.0);
	brightness = 10;

	N = nv;
	pos = pv;

	outColor = vec4(shade(),0.0);   
}
