#version 330 core
#define M_PI 3.1415926535897932384626433832795

uniform sampler2D colorTex;
uniform sampler2D emiTex;
uniform sampler2D specularTex;
uniform sampler2D normalTex;
uniform mat4 view;
uniform mat4 modelView;
uniform mat4 normal;
uniform mat4 model;

in vec3 pv;
in vec3 nv;
in vec3 color;
in vec2 texCoord;
in vec3 T;
//in vec3 B;

out vec4 outColor;

//Parametros de la luz puntual
vec3 point_Il = vec3(1.0);
vec3 point_Ia = vec3(0.05);
vec3 point_pl = vec3(0,0,0);
float light_range = 20f;
float point_IsFactor = 0;

//Parametros de la luz direccional
vec3 directional_dir = (view * vec4(normalize(vec3(1f, 0f,0f)),0)).xyz;
vec3 directional_Il = vec3(0,1,0);
vec3 directional_Ia = vec3(0,0.2,0);
float directional_IsFactor = 0;

//Parametros del la luz focal
vec3 focal_pl = (view * vec4(vec3(0,0,6),1)).xyz;
vec3 focal_dir = normalize((view * vec4(vec3(0,0,1),0)).xyz); //posicion de la luz - posicion del cubo
float focal_angle = 5f;
vec3 focal_Il = vec3(0,0,1);
vec3 focal_Ia = vec3(0,0,0.05);
float focal_light_range = 20f;
float focal_IsFactor = 0;

//Fog
float fog_density = 0.1f;
vec4 fog_color = vec4 (0.2, 0.2, 0.2, 0);

//Obj
vec3 Ka;
vec3 Kd;
vec3 Ks;
float brightness;
vec3 Ke;
vec3 pos;

vec3 N;
vec3 B;
mat3 TBN;

float attenuation (float c1, float c2, float c3, float d)
{
	return min(1/(c1 + c2 * d + c3 * pow(d, 2)), 1);
}

vec3 shade()
{
	vec3 c = vec3(0);

	//
	//Luz posicional
	//

		c+= point_Ia*Ka;

		//N = TBN * N;

		//Atenuación de la luz
		//float distanceToLight = length(point_pl-pos);
		//point_Il = point_Il * attenuation (0.1, 0.1, 0.1, distanceToLight);

		vec3 L = normalize(point_pl-pos);
		c += clamp (point_Il*Kd*dot(N,L),0.0,1.0);

		vec3 V = normalize(-pos);
		vec3 R = normalize (reflect(-L,N));
		point_IsFactor = clamp (dot(V,R), 0.0, 1.0);
		point_IsFactor = pow(point_IsFactor, brightness);
		c+= point_Il*Ks*point_IsFactor;
	
	//fin Luz posicional
	
	//
	//Luz direccional
	//
		//luz ambiental
		//c+= directional_Ia*Ka;

		//luz difusa
		//c += clamp (directional_Il*Kd*dot(N,directional_dir),0.0,1.0);

		//luz especular
		R = normalize (reflect(-directional_dir,N));
		directional_IsFactor = clamp (dot(V,R), 0.0, 1.0);
		point_IsFactor = pow(directional_IsFactor, brightness);
		//c += directional_Il*Ks*directional_IsFactor;

	//fin luz direccional

	//
	//Luz focal
	//
		//c+= focal_Ia*Ka;
		 
//		L = normalize(focal_pl-pos);
//		float current_angle = acos(dot(focal_dir, L))* (180f/M_PI);
//
//		if (current_angle < focal_angle)
//		{
//			//Atenuación de la luz
//			distanceToLight = length(focal_pl-pos);
//			focal_Il = focal_Il * attenuation (0.001, 0.001, 0.001, distanceToLight);
//
//		
//			//c += clamp (focal_Il*Kd*dot(N,L),0.0,1.0);
//
//			R = normalize (reflect(-L,N));
//			focal_IsFactor = clamp (dot(V,R), 0.0, 1.0);
//			focal_IsFactor = pow(focal_IsFactor, brightness);
//			//c+= focal_Il*Ks*focal_IsFactor;
//		}

	//fin luz focal

	//Luz emisiva 
	//c += Ke;

	return c;
}

void main()
{
	Ka = texture (colorTex, texCoord).rgb;
	Kd = Ka;
	Ks = texture(specularTex, texCoord).rgb;
	brightness = 20;
	Ke = texture(emiTex, texCoord).rgb;

	//N = normalize((normal * vec4(texture(normalTex, texCoord).rgb, 0)).xyz);
	 
	pos = pv;
	
	B = cross(nv, T);

	TBN = mat3(T,B,nv);

	N = TBN * texture(normalTex, texCoord).rgb;

	float f = exp(-pow(fog_density * pos.z, 2));

	outColor = vec4(shade(),0.0) * f + (1 - f) * fog_color;   
}