
#include "Shader.h"

void Shader::loadFragmentShader(std::string fileName)
{
	if (!lockedFragment)
	{
		fragmentFileName = fileName;

		unsigned int fileLength;
		char *source = loadStringFromFile(fileName.c_str(), fileLength);
		fragmentId = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragmentId, 1,
			(const GLchar **)&source, (const GLint *)&fileLength);
		glCompileShader(fragmentId);
		delete[] source;

		//Comprobamos que se compil� bien
		GLint compiled;
		glGetShaderiv(fragmentId, GL_COMPILE_STATUS, &compiled);
		if (!compiled)
		{
			//Calculamos una cadena de error
			GLint logLen;
			glGetShaderiv(fragmentId, GL_INFO_LOG_LENGTH, &logLen);
			char *logString = new char[logLen];
			glGetShaderInfoLog(fragmentId, logLen, NULL, logString);
			std::cout << "Error in " << fileName << ": " << logString << std::endl;
			delete[] logString;
			glDeleteShader(fragmentId);
			exit(-1);
		}

		lockedFragment = true;
	}
	else
	{
		std::cout << "Tried to overwrite vertex shader: " << fragmentFileName <<
			" with: " << fileName << std::endl;
	}
}

void Shader::loadVertexShader(std::string fileName)
{
	if (!lockedVertex)
	{
		vertexFileName = fileName;

		unsigned int fileLength;
		char *source = loadStringFromFile(fileName.c_str(), fileLength);
		vertexId = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertexId, 1,
			(const GLchar **)&source, (const GLint *)&fileLength);
		glCompileShader(vertexId);
		delete[] source;

		//Comprobamos que se compil� bien
		GLint compiled;
		glGetShaderiv(vertexId, GL_COMPILE_STATUS, &compiled);
		if (!compiled)
		{
			//Calculamos una cadena de error
			GLint logLen;
			glGetShaderiv(vertexId, GL_INFO_LOG_LENGTH, &logLen);
			char *logString = new char[logLen];
			glGetShaderInfoLog(vertexId, logLen, NULL, logString);
			std::cout << "Error in " << fileName << ": " << logString << std::endl;
			delete[] logString;
			glDeleteShader(vertexId);
			exit(-1);
		}
		lockedVertex = true;
	}
	else
	{
		std::cout << "Tried to overwrite vertex shader: " << vertexFileName <<
			" with: " << fileName << std::endl;
	}
}

void Shader::link()
{
	programId = glCreateProgram();
	glAttachShader(programId, vertexId);
	glAttachShader(programId, fragmentId);
	glLinkProgram(programId); 

	int linked;
	glGetProgramiv(programId, GL_LINK_STATUS, &linked);
	if (!linked)
	{
		//Calculamos una cadena de error
		GLint logLen;
		glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &logLen);
		char *logString = new char[logLen];
		glGetProgramInfoLog(programId, logLen, NULL, logString);
		std::cout << "Error: " << logString << std::endl;
		delete[] logString;
		glDeleteProgram(programId);
		programId = 0;
		exit(-1);
	}


}

void Shader::setUniformMatrix4(int id, glm::mat4 &value)
{
	glUniformMatrix4fv(id, 1, GL_FALSE,	&(value[0][0]));
	
}

void Shader::setUniformVec3D(int id, glm::vec3 &value)
{
	glUniform3f(id, value.x, value.y, value.z);
}


