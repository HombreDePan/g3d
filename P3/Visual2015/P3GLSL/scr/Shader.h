#ifndef __SHADER__
#define __SHADER__

#include <gl/glew.h>
#include <string>
#include "auxiliar.h"
#include <iostream>
#include <map>
#include <glm/glm.hpp>

class Shader
{
private:
	unsigned int vertexId, fragmentId, programId;
	bool lockedVertex = false, lockedFragment = false, lockedProgram = false; //Una vez creado el programa no se puede modificar
	std::string vertexFileName, fragmentFileName;
	int uNormalMat, uModelViewMat, uModelViewProjMat;
	int inPos, inColor, inNormal, inTexCoord;

	//std::map<std::string, int> uniforms;

public:


	Shader() {}

	//TODO: Crear constructor que acepte los nombres de los shaders a cargar

	void loadVertexShader(std::string fileName);

	void loadFragmentShader(std::string fileName);

	void link();

	void setUniformMatrix4(int id, glm::mat4 &value);

	void setUniformVec3D(int id, glm::vec3 & value);

	void setUniformInt(int id, int value);

	void setUniformFloat(int id, glm::mat4 &value);

	void setUniformVec3D(int id, glm::mat4 &value);
	

	void bindAttribLocation(int attribId, std::string attribName)
	{
		glBindAttribLocation(programId, attribId, attribName.c_str());
	}

	int getAttribLocation(std::string attribName)
	{
		return glGetAttribLocation(programId, attribName.c_str());
	}

	int getUniformLocation(std::string uniformName)
	{
		return glGetUniformLocation(programId, uniformName.c_str());
	}

	void useProgram()
	{
		glUseProgram(programId);
	}

	void initAttribIndex()
	{
		inPos = getAttribLocation("inPos");
		inColor = getAttribLocation("inColor");
		inNormal = getAttribLocation("inNormal");
		inTexCoord = getAttribLocation("inTexCoord");
	}

	int getInPos()
	{
		return inPos;
	}

	int getInColor()
	{
		return inColor;
	}

	int getInNormal()
	{
		return inNormal;
	}

	int getInTexCoord()
	{
		return inTexCoord;
	}
};



#endif