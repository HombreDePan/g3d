#include "BOX.h"
#include "Shader.h"
#include "auxiliar.h"

#include <windows.h>

#include <gl/glew.h>
#include <gl/gl.h>
#define SOLVE_FGLUT_WARNING
#include <gl/freeglut.h> 
#include <iostream>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>




//////////////////////////////////////////////////////////////
// Datos que se almacenan en la memoria de la CPU
//////////////////////////////////////////////////////////////

//Matrices
glm::mat4	proj = glm::mat4(1.0f);
glm::mat4	view = glm::mat4(1.0f);
glm::mat4	model = glm::mat4(1.0f);


//////////////////////////////////////////////////////////////
// Variables que nos dan acceso a Objetos OpenGL
//////////////////////////////////////////////////////////////
//Por definir
unsigned int vshader;
unsigned int fshader;
unsigned int program;
Shader shader_v0;
Shader shader_v1;

//Variables Uniform
int uModelViewMat_1;
int uModelViewProjMat_1;
int uNormalMat_1;

int uModelViewMat_2;
int uModelViewProjMat_2;
int uNormalMat_2;

int uColorTex;
int uEmiTex;

//Variables de las luces
int diffuse_light_intensity_1;
glm::vec3 u_diffuse_light(1.0);

int diffuse_light_intensity_2;

//Atributos
int inPos;
int inColor;
int inNormal;
int inTexCoord;

//VAO
unsigned int vao;
//VBOs que forman parte del objeto
unsigned int posVBO;
unsigned int colorVBO;
unsigned int normalVBO;
unsigned int texCoordVBO;
unsigned int triangleIndexVBO;

//TExturas
unsigned int colorTexId;
unsigned int emiTexId;

//////////////////////////////////////////////////////////////
// Funciones auxiliares
//////////////////////////////////////////////////////////////
//!!Por implementar

//Declaraci�n de CB
void renderFunc();
void resizeFunc(int width, int height);
void idleFunc();
void keyboardFunc(unsigned char key, int x, int y);
void mouseFunc(int button, int state, int x, int y);

//Funciones de inicializaci�n y destrucci�n
void initContext(int argc, char** argv);
void initOGL();
void initShader(const char *vname, const char *fname, Shader& shader);
void initAttributes();
void initUniforms();
void initObj();
void destroy();


//Carga el shader indicado, devuele el ID del shader
//!Por implementar
GLuint loadShader(const char *fileName, GLenum type);

//Crea una textura, la configura, la sube a OpenGL, 
//y devuelve el identificador de la textura 
//!!Por implementar
unsigned int loadTex(const char *fileName);


int main(int argc, char** argv)
{
	std::locale::global(std::locale("spanish"));// acentos ;)

	initContext(argc, argv);
	initOGL();
	initShader("../shaders_P3/shader.v0.vert", "../shaders_P3/shader.v0.frag", shader_v0);
	initShader("../shaders_P3/shader.v1.vert", "../shaders_P3/shader.v1.frag", shader_v1);	
	initUniforms();
	initAttributes();
	initObj();

	glutMainLoop();

	destroy();

	return 0;
}
	
//////////////////////////////////////////
// Funciones auxiliares 
void initContext(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitContextVersion(3, 3);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
	glutInitContextProfile(GLUT_CORE_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("Pr�cticas OGL");

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		std::cout << "Error: " << glewGetErrorString(err) << std::endl;
		exit(-1);
	}
	const GLubyte *oglVersion = glGetString(GL_VERSION);
	std::cout << "This system supports OpenGL Version: " << oglVersion << std::endl;

	glutReshapeFunc(resizeFunc);
	glutDisplayFunc(renderFunc);
	glutIdleFunc(idleFunc);
	glutKeyboardFunc(keyboardFunc);
	glutMouseFunc(mouseFunc);
}

void initOGL()
{
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.2f, 0.2f, 0.2f, 0.0f);

	glFrontFace(GL_CCW);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_CULL_FACE);

	proj = glm::perspective(glm::radians(60.0f), 1.0f, 0.1f, 50.0f);
	view = glm::mat4(1.0f);
	view[3].z = -6;
}

void destroy()
{
	glDetachShader(program, vshader);
	glDetachShader(program, fshader);
	glDeleteShader(vshader);
	glDeleteShader(fshader);
	glDeleteProgram(program);

	if (inPos != -1) glDeleteBuffers(1, &posVBO);
	if (inColor != -1) glDeleteBuffers(1, &colorVBO);
	if (inNormal != -1) glDeleteBuffers(1, &normalVBO);
	if (inTexCoord != -1) glDeleteBuffers(1, &texCoordVBO);
	glDeleteBuffers(1, &triangleIndexVBO);
	glDeleteVertexArrays(1, &vao);
	glDeleteTextures(1, &colorTexId);
	glDeleteTextures(1, &emiTexId);
}

void initShader(const char *vname, const char *fname, Shader & shader)
{
	shader.loadVertexShader(vname);
	shader.loadFragmentShader(fname);

	shader.bindAttribLocation(0, "inPos");
	shader.bindAttribLocation(1, "inColor");
	shader.bindAttribLocation(2, "inNormal");
	shader.bindAttribLocation(3, "inTexCoord");

	shader.link();
}

void initAttributes()
{
	shader_v0.initAttribIndex();
	shader_v1.initAttribIndex();
}

void initUniforms()
{
	uNormalMat_1 = shader_v0.getUniformLocation("normal");
	uModelViewMat_1 = shader_v0.getUniformLocation("modelView");
	uModelViewProjMat_1 = shader_v0.getUniformLocation("modelViewProj");
	diffuse_light_intensity_1 = shader_v0.getUniformLocation("diffuse_light_intensity");

	uNormalMat_2 = shader_v1.getUniformLocation("normal");
	uModelViewMat_2 = shader_v1.getUniformLocation("modelView");
	uModelViewProjMat_2 = shader_v1.getUniformLocation("modelViewProj");
	diffuse_light_intensity_2 = shader_v1.getUniformLocation("diffuse_light_intensity");

	uColorTex = shader_v1.getUniformLocation("colorTex");
	uEmiTex = shader_v1.getUniformLocation("emiTex");
}

void initObj()
{
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	if (shader_v0.getInPos() != -1)
	{
		glGenBuffers(1, &posVBO);
		glBindBuffer(GL_ARRAY_BUFFER, posVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex * sizeof(float) * 3,
			cubeVertexPos, GL_STATIC_DRAW);
		glVertexAttribPointer(shader_v0.getInPos(), 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(shader_v0.getInPos());
	}
	if (shader_v0.getInColor() != -1)
	{
		glGenBuffers(1, &colorVBO);
		glBindBuffer(GL_ARRAY_BUFFER, colorVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex * sizeof(float) * 3,
			cubeVertexColor, GL_STATIC_DRAW);
		glVertexAttribPointer(shader_v0.getInColor(), 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(shader_v0.getInColor());
	}
	if (shader_v0.getInNormal() != -1)
	{
		glGenBuffers(1, &normalVBO);
		glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex * sizeof(float) * 3,
			cubeVertexNormal, GL_STATIC_DRAW);
		glVertexAttribPointer(shader_v0.getInNormal(), 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(shader_v0.getInNormal());
	}
	if (shader_v1.getInTexCoord() != -1)
	{
		glGenBuffers(1, &texCoordVBO);
		glBindBuffer(GL_ARRAY_BUFFER, texCoordVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex * sizeof(float) * 2,
			cubeVertexTexCoord, GL_STATIC_DRAW);
		glVertexAttribPointer(shader_v1.getInTexCoord(), 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(shader_v1.getInTexCoord());
	}

	glGenBuffers(1, &triangleIndexVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleIndexVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		cubeNTriangleIndex * sizeof(unsigned int) * 3, cubeTriangleIndex,
		GL_STATIC_DRAW);

	model = glm::mat4(1.0f);

	colorTexId = loadTex("../img/color2.png");
	emiTexId = loadTex("../img/emissive.png");
}

GLuint loadShader(const char *fileName, GLenum type)
{
	unsigned int fileLen;
	char *source = loadStringFromFile(fileName, fileLen);
	//////////////////////////////////////////////
	//Creaci�n y compilaci�n del Shader
	GLuint shader;
	shader = glCreateShader(type);
	glShaderSource(shader, 1,
		(const GLchar **)&source, (const GLint *)&fileLen);
	glCompileShader(shader);
	delete[] source;

	//Comprobamos que se compil� bien
	GLint compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	if (!compiled)
	{
		//Calculamos una cadena de error
		GLint logLen;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLen);
		char *logString = new char[logLen];
		glGetShaderInfoLog(shader, logLen, NULL, logString);
		std::cout << "Error in " << fileName << ": " << logString << std::endl;
		delete[] logString;
		glDeleteShader(shader);
		exit(-1);
	}

	return shader; 
}
unsigned int loadTex(const char *fileName){ return 0; }

void renderFunc()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//glUseProgram(program);
	shader_v0.useProgram();

	glm::mat4 modelView = view * model;
	glm::mat4 modelViewProj = proj * view * model;
	glm::mat4 normal = glm::transpose(glm::inverse(modelView));
	if (uModelViewMat_1 != -1)
		glUniformMatrix4fv(uModelViewMat_1, 1, GL_FALSE,&(modelView[0][0]));
		//shader_v0.setUniformMatrix4(uModelViewMat_1, modelView);
	if (uModelViewProjMat_1 != -1)
		glUniformMatrix4fv(uModelViewProjMat_1, 1, GL_FALSE,
			&(modelViewProj[0][0]));
	if (uNormalMat_1 != -1)
		glUniformMatrix4fv(uNormalMat_1, 1, GL_FALSE,
			&(normal[0][0]));
	if (diffuse_light_intensity_1 != -1)
	{
		glUniform3f(diffuse_light_intensity_1, u_diffuse_light.r, u_diffuse_light.g, u_diffuse_light.b);
	}

	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, cubeNTriangleIndex * 3,
		GL_UNSIGNED_INT, (void*)0);

	shader_v1.useProgram();
	glm::mat4 model2(1.0);
	model2 = glm::translate(model2, glm::vec3(5.0, 0.0, 0.0));
	modelView = view * model2;
	modelViewProj = proj * view * model2;
	normal = glm::transpose(glm::inverse(modelView));

	if (uModelViewMat_2 != -1)
		glUniformMatrix4fv(uModelViewMat_2, 1, GL_FALSE,
			&(modelView[0][0]));
	if (uModelViewProjMat_2 != -1)
		glUniformMatrix4fv(uModelViewProjMat_2, 1, GL_FALSE,
			&(modelViewProj[0][0]));
	if (uNormalMat_2 != -1)
		glUniformMatrix4fv(uNormalMat_2, 1, GL_FALSE,
			&(normal[0][0]));
	if (diffuse_light_intensity_2 != -1)
	{
		glUniform3f(diffuse_light_intensity_2, u_diffuse_light.r, u_diffuse_light.g, u_diffuse_light.b);
	}
	if (uColorTex != -1)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, colorTexId);
		glUniform1i(uColorTex, 0);
	}

	if (uEmiTex != -1)
	{
		glActiveTexture(GL_TEXTURE0 + 1);
		glBindTexture(GL_TEXTURE_2D, emiTexId);
		glUniform1i(uEmiTex, 1);
	}

	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, cubeNTriangleIndex * 3,
		GL_UNSIGNED_INT, (void*)0);

	glutSwapBuffers();
}

void resizeFunc(int width, int height)
{
	glViewport(0, 0, width, height);

	//Ajusta el aspect ratio al tama�o de la venta
	float n = 1.0f;
	float f = 10.0f;
	float aspect_ratio = (float)width / (float)height;
	float fov = 3.14159f / 3; //60 grados
	float top = tan(fov / 2) * n;
	float bottom = -top;
	float right = top * aspect_ratio;
	float left = -top * aspect_ratio;

	proj = glm::mat4(1.0);
	proj[0].x = 2 * n / (right - left);
	proj[0].z = (right + left) / (right - left);
	proj[1].y = 2 * n / (top - bottom);
	proj[1].z = (top + bottom) / (top - bottom);
	proj[2].z = -(f + n) / (f - n);
	proj[2].w = -2 * f * n / (f - n);
	proj[3].z = -1;	
}

void idleFunc()
{
	model = glm::mat4(1.0f);
	static float angle = 0.0f;
	angle = (angle > 3.141592f * 2.0f) ? 0 : angle + 0.01f;
	model = glm::rotate(model, angle, glm::vec3(1.0f, 1.0f, 0.0f));
	glutPostRedisplay();
}

void keyboardFunc(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'w':
		if (u_diffuse_light.x < 1)
		{
			u_diffuse_light = u_diffuse_light + glm::vec3(0.1);
		}
		break;
	case 's':
		if (u_diffuse_light.x > 0)
		{
			u_diffuse_light = u_diffuse_light - glm::vec3(0.1);
		}
		break;
	default:
		break;
	}
}
void mouseFunc(int button, int state, int x, int y){}









