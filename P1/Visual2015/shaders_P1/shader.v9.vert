#version 330 core

in vec3 inPos;
in vec3 inNormal;
in vec2 inTexCoord;	

uniform mat4 modelViewProj;
uniform mat4 normal;

out vec3 n;
out vec2 texCoord;

void main()
{
	texCoord = inTexCoord;
	n = (normal * vec4 (inNormal, 0.0)).xyz;
	gl_Position =   modelViewProj *
		vec4 (inPos,1.0);
}
