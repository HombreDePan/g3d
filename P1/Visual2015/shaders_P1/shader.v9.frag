#version 330 core

out vec4 outColor;
in vec3 n;
in vec2 texCoord;

void main()
{
	if ( pow(texCoord.x - 0.5f, 2) + pow(texCoord.y - 0.5f, 2) < 0.2f)
	{
		outColor = abs(vec4(n,0));   

	}
	else{
		discard;
		}
}
