#version 330 core

in vec3 inPos;		

mat4 view;
mat4 proj;

void main()
{
	view = mat4(1.0);
	view[3].z = -3.0;

	float n = 1.0;
	float f = 10.0;
	proj = mat4(0.0);
	proj[0].x = 1.0/tan(3.14159/6.0);
	proj[1].y = proj[0].x;
	proj[2].z = -(n+f)/(f-n);
	proj[3].z = - 2.0 * f * n /(f-n);
	proj[2].w = -1;



	gl_Position =  proj * view * vec4 (inPos,1.0);
}
