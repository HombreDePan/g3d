#version 330 core

in vec3 inPos;
in vec3 inColor;
in vec2 inTexCoord;	

uniform mat4 modelViewProj;
out vec4 color;
out vec2 texCoord;

void main()
{
	color = vec4 (inColor,0.0);
	texCoord = inTexCoord;

	gl_Position =   modelViewProj *
		vec4 (inPos,1.0);
}
