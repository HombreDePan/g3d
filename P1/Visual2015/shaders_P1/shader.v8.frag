#version 330 core

out vec4 outColor;
in vec3 n;

void main()
{
	outColor = abs(vec4(n,0));   
}
