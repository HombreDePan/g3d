#version 330 core

in vec3 inPos;		
uniform mat4 modelViewProj;
out vec4 color;

void main()
{
	color = (mod(gl_VertexID,2)==0)? vec4(1.0): vec4(1.0,vec3(0.0));

	gl_Position =   modelViewProj *
		vec4 (inPos,1.0);
}
