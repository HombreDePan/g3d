#include "BOX.h"
#include <IGL/IGlib.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <vector>
#include <math.h>
# define M_PI           3.14159265358979323846


//Idenficadores de los objetos de la escena
int objId = -1;
int segundoCubo = -1;
int assimpMesh = -1;
//Propiedades c�rculo para el cubo orbital
float radius = 5;
float x_origin = 0;
float y_origin = 0;
float x = 0;
float y = 0;
//Movimiento de la c�mara
glm::vec3 cam_pos(0.0f, 0.0f, 6.0f);
glm::vec3 cam_forward(0.0f, 0.0f, -1.0f);
glm::vec3 cam_up(0.0f, 1.0f, 0.0f);

float rotation_speed = 0.05f;
float speed = 0.1f;
float cam_horizontal_rotation = 0;
float cam_vertical_rotation = 0;

//Declaraci�n de CB
void resizeFunc(int width, int height);
void idleFunc();
void keyboardFunc(unsigned char key, int x, int y);
void mouseFunc(int button, int state, int x, int y);
void mouseMotionFunc(int x, int y);

//C�mara orbital
bool mouseClicked = false;
int mouse_x_ref;
int mouse_y_ref;
float theta;
float phi;
float camera_radius = 6;


int main(int argc, char** argv)
{
	std::locale::global(std::locale("spanish"));// acentos ;)
	if (!IGlib::init("../shaders_P1/shader.v8.vert", "../shaders_P1/shader.v8.frag"))
		return -1;

	//CBs
	IGlib::setResizeCB(resizeFunc);
	IGlib::setIdleCB(idleFunc);
	IGlib::setKeyboardCB(keyboardFunc);
	IGlib::setMouseCB(mouseFunc);
	IGlib::setMouseMoveCB(mouseMotionFunc);

	//Se ajusta la c�mara
	float n = 1.0f;
	float f = 10.0f;
	glm::mat4 view(1.0f);
	glm::mat4 proj(0.0f);

	view = glm::lookAt(cam_pos, cam_forward, cam_up);

	proj[0].x = 1.0f /
		tan(3.14159f / 6.0f);
	proj[1].y = proj[0].x;
	proj[2].z = -(n + f) / (f - n);
	proj[3].z = -2.0f * f * n / (f - n);
	proj[2].w = -1.0f;


	IGlib::setProjMat(proj);
	IGlib::setViewMat(view);
	glm::mat4 modelMat = glm::mat4(1.0f);
	//Creamos el objeto que vamos a visualizar
	//Cubo 1
	objId = IGlib::createObj(cubeNTriangleIndex, cubeNVertex, cubeTriangleIndex,
			cubeVertexPos, cubeVertexColor, cubeVertexNormal,cubeVertexTexCoord, cubeVertexTangent);

	/*glm::mat4*/ modelMat = glm::mat4(1.0f);
	IGlib::setModelMat(objId, modelMat);
	IGlib::addColorTex(objId, "../img/color.png");
	//Cubo 2
	segundoCubo = IGlib::createObj(cubeNTriangleIndex, cubeNVertex, cubeTriangleIndex,
		cubeVertexPos, cubeVertexColor, cubeVertexNormal, cubeVertexTexCoord, cubeVertexTangent);

	modelMat = glm::mat4(1.0f);
	IGlib::setModelMat(segundoCubo, modelMat);
	IGlib::addColorTex(segundoCubo, "../img/color.png");
	//Incluir texturas aqu�.

	//Importamos malla usando assimp

	// Create an instance of the Importer class
	Assimp::Importer importer;
	// And have it read the given file with some example postprocessing
	// Usually - if speed is not the most important aspect for you - you'll
	// probably to request more postprocessing than we do in this example.
	const aiScene* scene = importer.ReadFile("../meshes/bunny.ply",
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType |
		aiProcess_GenNormals);
	// If the import failed, report it
	/*if (!scene)
	{
	std::cout << importer.GetErrorString();
	return NULL;
	}*/
	// We're done. Everything will be cleaned up by the importer destructor

	const aiMesh* mesh = scene->mMeshes[0];
	aiVector3D* vertex = mesh->mVertices;
	std::vector<float> vertices;
	for (size_t i = 0; i < mesh->mNumVertices; i++)
	{
		vertices.push_back(vertex[i].x);
		vertices.push_back(vertex[i].y);
		vertices.push_back(vertex[i].z);
	}

	aiFace* original_face = mesh->mFaces;
	std::vector<unsigned int> faces;
	for (size_t i = 0; i < mesh->mNumFaces; i++)
	{
		faces.push_back(original_face[i].mIndices[0]);
		faces.push_back(original_face[i].mIndices[1]);
		faces.push_back(original_face[i].mIndices[2]);
	}

	aiVector3D* original_normal = mesh->mNormals;
	std::vector<float> normals;
	for (size_t i = 0; i < mesh->mNumVertices; i++)
	{
		normals.push_back(original_normal[i].x);
		normals.push_back(original_normal[i].y);
		normals.push_back(original_normal[i].z);
	}

	assimpMesh = IGlib::createObj(mesh->mNumFaces, mesh->mNumVertices, faces.data(),
		vertices.data(), NULL, normals.data(), NULL, NULL);
	modelMat = glm::mat4(1.0f);

	modelMat = glm::scale(modelMat, glm::vec3(0.1, 0.1, 0.1));
	IGlib::setModelMat(assimpMesh, modelMat);
	//Mainloop
	IGlib::mainLoop();
	IGlib::destroy();
	return 0;
}

void resizeFunc(int width, int height)
{
	//Ajusta el aspect ratio al tama�o de la venta
	float n = 1.0f;
	float f = 10.0f;
	float aspect_ratio = (float)width / (float)height;
	float fov = 3.14159f / 3; //60 grados
	float top = tan(fov / 2) * n;
	float bottom = -top;
	float right = top * aspect_ratio;
	float left = -top * aspect_ratio;

	glm::mat4 proj(0.0f);
	proj[0].x = 2 * n / (right - left);
	proj[0].z = (right + left) / (right - left);
	proj[1].y = 2 * n / (top - bottom);
	proj[1].z = (top + bottom) / (top - bottom);
	proj[2].z = -(f + n) / (f - n);
	proj[2].w = -2 * f * n / (f - n);
	proj[3].z = -1;

	IGlib::setProjMat(proj);
}

void idleFunc()
{
	////Cubo central
	//static float angle = 0.0f;
	//angle = (angle<2.0f*3.14159f)?
	//	angle + 0.01f : 0.0f;
	//glm::mat4 model(1.0f);
	//model = glm::rotate(model, angle, glm::vec3(1.0f, 1.0f, 0.0f));
	//IGlib::setModelMat(objId, model);

	//Cubo orbital
	static float angleOrbit_cubo2 = 0.0f;
	angleOrbit_cubo2 = (angleOrbit_cubo2 < 2.0f*3.14159f) ?
		angleOrbit_cubo2 + 0.01f : 0.0f;
	glm::mat4 model2(1.0f);

	//Traslaci�n del cubo orbital
	model2 = glm::translate(model2, glm::vec3(x, y, 0));
	x = x_origin + radius * cos(angleOrbit_cubo2);
	y = y_origin + radius * sin(angleOrbit_cubo2);
	//Rotaci�n del cubo orbital
	static float angleSelfRotation_cubo2 = 0.0f;
	angleSelfRotation_cubo2 = (angleSelfRotation_cubo2 < 2.0f*3.14159f) ?
		angleSelfRotation_cubo2 + 0.05f : 0.0f;
	model2 = glm::rotate(model2, angleSelfRotation_cubo2, glm::vec3(0.0f, 1.0f, 0.0f));

	IGlib::setModelMat(segundoCubo, model2);
}

void keyboardFunc(unsigned char key, int x, int y)
{
	std::cout << "Se ha pulsado la tecla " << key << std::endl << std::endl;

	glm::mat4 view(1.0f);

	if (key == 'w')
	{
		//Adelante
		cam_pos += speed * cam_forward;
	}
	if (key == 'a')
	{
		//Izquierda
		cam_pos -= glm::normalize(glm::cross(cam_forward, cam_up)) * speed;
	}
	if (key == 's')
	{
		//Atr�s
		cam_pos -= speed * cam_forward;
	}
	if (key == 'd')
	{
		//Derecha
		cam_pos += glm::normalize(glm::cross(cam_forward, cam_up)) * speed;
	}
	if (key == 'e')
	{
		//Rotaci�n derecha
		//cam_forward = rotation_speed;
		cam_forward = glm::vec3(glm::rotate(glm::mat4(1.0f), -rotation_speed, glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(cam_forward, 0));
	}
	else if (key == 'q')
	{
		//Rotaci�n izquierda
		cam_horizontal_rotation -= rotation_speed;
		cam_forward = glm::vec3(glm::rotate(glm::mat4(1.0f), rotation_speed, glm::vec3(0.0f, 1.0f, 0.0f)) * glm::vec4(cam_forward, 0));
		//view = glm::rotate(view, cam_horizontal_rotation, glm::vec3(0.0f, 1.0f, 0.0f));
	}

	view = glm::lookAt(cam_pos, cam_pos + cam_forward, cam_up);

	IGlib::setViewMat(view);
}

void mouseFunc(int button, int state, int x, int y)
{
	if (state == 0)
	{
		std::cout << "Se ha pulsado el bot�n ";
		mouseClicked = true;
		mouse_x_ref = x;
		mouse_y_ref = y;
	}
	else
	{
		std::cout << "Se ha soltado el bot�n ";
		mouseClicked = false;
	}
	if (button == 0) std::cout << "de la izquierda del rat�n " << std::endl;
	if (button == 1) std::cout << "central del rat�n " << std::endl;
	if (button == 2) std::cout << "de la derecha del rat�n " << std::endl;

	std::cout << "en la posici�n " << x << " " << y << std::endl << std::endl;
}

void mouseMotionFunc(int x, int y)
{
	
	if (mouseClicked)
	{
		
		//camera_radius = cam_pos.length();
		int x_offset = x - mouse_x_ref;
		int y_offset = y - mouse_y_ref;

		std::cout << "X: " << x_offset << " Y: " << y_offset << std::endl;

		theta += glm::radians((float) x_offset  );
		phi += glm::radians((float) y_offset );

		float eye_x = camera_radius * cos(phi) * sin(theta);
		float eye_y = camera_radius * sin(phi) ;
		float eye_z = camera_radius * cos(phi) * cos(theta);

		IGlib::setViewMat(glm::lookAt(glm::vec3(eye_x, eye_y, eye_z), glm::vec3(0, 0, 0), cam_up));
		
		mouse_x_ref = x;
		mouse_y_ref = y;
	}
}